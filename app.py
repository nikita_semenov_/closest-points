from collections import namedtuple
from operator import attrgetter
from math import sqrt
from itertools import combinations

Point = namedtuple("point", ["x", "y"])


def dist(p1, p2):
    """
    In fact its square of distance. Which is good for sorting.
    """
    return (p1.x - p2.x) ** 2 + (p1.y - p2.y) ** 2


def closest_in_stripe(points):
    res = float("inf")
    for i, point_i in enumerate(points):
        for point_j in points[i + 1 : i + 7]:
            res = min(res, dist(point_i, point_j))
    return res


def brute_min_search(points):
    return min(dist(p1, p2) for (p1, p2) in combinations(points, 2))


def find_closest(xs):
    l = len(xs)
    if l < 16:
        return brute_min_search(xs)
    dist_left = find_closest(xs[: l // 2])
    dist_right = find_closest(xs[l // 2 :])
    dist = min(dist_left, dist_right)
    x_avr_coord = sum(map(attrgetter("x"), xs)) / l
    l_border = x_avr_coord - sqrt(dist)
    r_border = x_avr_coord + sqrt(dist)
    points_in_stripe = [
        point for point in xs if r_border < point.x < l_border
    ]
    points_in_stripe.sort(key=attrgetter("y"))
    d_in_stripe = closest_in_stripe(points_in_stripe)
    return min(dist, d_in_stripe)


def main():
    n = int(input())
    assert n in range(2, 100001)
    points = [Point(*map(int, input().split())) for _ in range(n)]
    points.sort(key=attrgetter("x"))
    print(sqrt(find_closest(points)))


if __name__ == "__main__":
    main()
